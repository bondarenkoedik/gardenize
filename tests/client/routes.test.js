describe('Testing Routes', function () {

    // load the controller's module
    beforeEach(module('gardenizeApp'));

    it('should test routes', inject(function ($route) {

      expect($route.routes['/'].controller).toBe('HomePageController');
      expect($route.routes['/'].templateUrl).toEqual('/static/partials/home.html');

      expect($route.routes['/questions'].controller).toBe('QuestionsController');
      expect($route.routes['/questions'].templateUrl).toEqual('/static/partials/questions/list.html');

      expect($route.routes['/questions/filter/:type'].controller).toBe('QuestionsController');
      expect($route.routes['/questions/filter/:type'].templateUrl).toEqual('/static/partials/questions/list.html');

      expect($route.routes['/questions/new'].controller).toBe('QuestionsController');
      expect($route.routes['/questions/new'].templateUrl).toEqual('/static/partials/questions/new.html');

      expect($route.routes['/questions/:id'].controller).toBe('QuestionDetailsController');
      expect($route.routes['/questions/:id'].templateUrl).toEqual('/static/partials/questions/show.html');

      expect($route.routes[null].redirectTo).toEqual('/');
    
    }));

});