describe("HomePageController", function() {

	var scope, ctrl;

	beforeEach(module('gardenizeApp'));

	beforeEach(inject(['$controller', '$rootScope', function($controller, $rootScope) {
        scope = $rootScope.$new();
		ctrl = $controller('HomePageController', { $scope: scope });
    }]));

	it("should show home page", function() {
		expect(ctrl.helloMessage).toBeDefined();
	});

});