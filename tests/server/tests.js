'use strict'

let request = require('supertest');
let should = require('should');

let Question = require('../../models/Question');

let app = require('../../app');


describe('test home page', function() {

    it('should return home page', (done) => {
        request(app)
            .get('/')
            .expect('Content-type', /html/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                done();
            });
    });

});

describe('test API', function() {

    let firstQuestionId;
    let secondQuestionId;
    let thirdQuestionId;

    afterEach(done => {
        
        // looks good even without promise
        Question.remove({}, (err) => {
            return done(err);
        });

    });

    beforeEach(done => {


        let firstQuestionPromise = Question.create({
            title: 'Title for test question 1',
            body: 'Body for test question 1',
            author: 'User'
        });

        let secondQuestionPromise = Question.create({
            title: 'Title for test question 2',
            body: 'Body for test question 2',
            author: 'User',
            comments: [
                { author: 'User 2', body: 'good question.' },
                { author: 'User 2', body: 'good question.' }
            ]
        });

        let thirdQuestionPromise = Question.create({
            title: 'Title for test question 3',
            body: 'Body for test question 3',
            author: 'User',
            comments: [
                { author: 'User 2', body: 'good question.' },
                { author: 'User 2', body: 'good question.' },
                { author: 'User 2', body: 'good question.' }
            ]
        });

        Promise.all([
            firstQuestionPromise,
            secondQuestionPromise,
            thirdQuestionPromise
        ]).then(
            result => {
                firstQuestionId = result[0]._id;
                secondQuestionId = result[1]._id;
                thirdQuestionId = result[2]._id;
                done();
            },
            error => {
                console.log(error);
                process.exit(1);
            }
        );
    });

    // TESTS

    it('GET /questions', (done) => {
        request(app)
            .get('/questions')
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                res.body.should.be.empty;
                done();
            });
    });

    it('GET /questions/filter/answered', (done) => {
        request(app)
            .get('/questions/filter/answered')
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                res.body.should.have.length(2);
                done();
            })
    });


    it('GET /questions/filter/unanswered', (done) => {
        request(app)
            .get('/questions/filter/unanswered')
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                res.body.should.have.length(1);
                done();
            })
    });

    it('GET /questions/filter/errorfilter', (done) => {
        request(app)
            .get('/questions/filter/errorfilter')
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                res.body.should.have.length(3);
                done();
            });
    });

    it('POST /questions. Incorrect question data.', (done) => {
        request(app)
            .post('/questions')
            .send({})
            .expect('Content-type', /json/)
            .expect(400, done);
    });

    it('POST /questions. Correct question data.', (done) => {
        request(app)
            .post('/questions')
            .send({ author: '  user', body: '<script>alert("bad alert")</script><strong>body question</strong>', title: 'question title' })
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                res.body.should.have.property('_id');
                res.body.should.have.property('date');
                res.body.should.have.property('comments', []);

                res.body.author.should.equal('user');
                res.body.body.should.equal('<strong>body question</strong>');
                res.body.title.should.equal('question title');

                done();
            });
    });

    it('GET /question/:id. Incorrect id. First case.', (done) => {
        request(app)
            .get('/questions/errorquestionid')
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);
                
                should(res.body).not.be.ok;
                done();
            });
    });

    it('GET /question/:id. Incorrect id. Second case.', (done) => {
        request(app)
            .get('/questions/507c35dd8fada716c89d0013') // this is some random ObjectId
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                should(res.body).not.be.ok;

                done();
            });
    });

    it('GET /question/:id. Correct id. First case.', (done) => {
        request(app)
            .get(`/questions/${firstQuestionId}`)
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                should(res.body).be.ok;

                res.body.should.have.property('_id');
                res.body.should.have.property('title');
                res.body.should.have.property('body');
                res.body.should.have.property('author');
                res.body.should.have.property('date');
                res.body.should.have.property('comments', []);

                res.body.author.should.equal('User');
                res.body.body.should.equal('Body for test question 1');
                res.body.title.should.equal('Title for test question 1');

                done();

            });
    });

    it('GET /question/:id. Correct id. Second case.', (done) => {
        request(app)
            .get(`/questions/${secondQuestionId}`)
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                should(res.body).be.ok;

                res.body.should.have.property('_id');
                res.body.should.have.property('title');
                res.body.should.have.property('body');
                res.body.should.have.property('author');
                res.body.should.have.property('date');

                res.body.author.should.equal('User');
                res.body.body.should.equal('Body for test question 2');
                res.body.title.should.equal('Title for test question 2');
                res.body.comments.should.have.length(2);

                done();

            });
    });

    it('GET /question/:id. Correct id. Third case.', (done) => {
        request(app)
            .get(`/questions/${thirdQuestionId}`)
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                should(res.body).be.ok;

                res.body.should.have.property('_id');
                res.body.should.have.property('title');
                res.body.should.have.property('body');
                res.body.should.have.property('author');
                res.body.should.have.property('date');

                res.body.author.should.equal('User');
                res.body.body.should.equal('Body for test question 3');
                res.body.title.should.equal('Title for test question 3');
                res.body.comments.should.have.length(3);

                done();

            });
    });

    it('POST /question/:id/comments. Incorrect data question id.', (done) => {
        request(app)
            .post(`/questions/errorquestionid/comments`)
            .expect('Content-type', /json/)
            .expect(400, done);
    });

    it('POST /question/:id/comments. Incorrect comment data.', (done) => {
        request(app)
            .post(`/questions/${thirdQuestionId}/comments`)
            .send({})
            .expect('Content-type', /json/)
            .expect(400, done);
    });

    it('POST /question/:id/comments. Correct data.', (done) => {
        request(app)
            .post(`/questions/${firstQuestionId}/comments`)
            .send({ author: 'User 2', body: 'my best comment' })
            .expect('Content-type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return done(err);

                should(res.body).be.ok;

                res.body.should.have.property('_id');
                res.body.should.have.property('body');
                res.body.should.have.property('author');
                res.body.should.have.property('date');

                res.body.comments.should.have.length(1);
                res.body.comments[0].should.have.property('_id');
                res.body.comments[0].should.have.property('author', 'User 2');
                res.body.comments[0].should.have.property('body', 'my best comment');
                res.body.comments[0].should.have.property('date');

                done();

            });
    });

});