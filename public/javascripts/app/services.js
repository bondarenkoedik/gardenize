(function() {

    'use strict'

    angular.module('gardenizeApp').service('questionsService', ['$http', function($http) {

        this.getAllQuestions = function(filterType) {
            if (filterType) {
                return $http.get('/questions/filter/' + filterType).then(response => {
                    return response.data;
                });
            } else {
                return $http.get('/questions/').then(response => {
                    return response.data;
                });
            }
        }

        this.addQuestion = function(question) {
            return $http.post('/questions', question).then(response => {
                return response.data;
            });
        }

        this.getQuestion = function(questionId) {
            return $http.get('/questions/' + questionId).then(response => {
                return response.data;
            });
        }

        this.addComment = function(questionId, comment) {
            return $http.post('/questions/' + questionId + '/comments', comment).then(response => {
                return response.data;
            });
        }

    }]);

}());