(function() {

    'use strict'

    angular.module('gardenizeApp').directive('comment', function() {
        return {
            restrict: 'E',
            templateUrl: '/static/partials/directives/comment.html',
            scope: {
                body: '=',
                author: '=',
                date: '=',
                dateFormat: '@'
            }
        }
    });

    angular.module('gardenizeApp').directive('question', function() {
        return {
            restrict: 'E',
            templateUrl: '/static/partials/directives/question.html',
            scope: {
                title: '=',
                body: '=',
                author: '=',
                date: '=',
                dateFormat: '@',
                comments: '=',
            }
        }
    });

}());