(function() {

    'use strict'

    angular.module('gardenizeApp').filter('sanitize', ['$sce', function($sce) {
      return function(htmlCode){
        return $sce.trustAsHtml(htmlCode);
      }
    }]);

}());