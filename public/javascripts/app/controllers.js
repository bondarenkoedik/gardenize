(function() {

    'use strict'

    angular.module('gardenizeApp').controller('HomePageController', [function() {
        this.helloMessage = 'Hi there!';
    }]);

    angular.module('gardenizeApp').controller('QuestionsController', [
                                            '$scope',
                                            '$location',
                                            '$routeParams',
                                            'questionsService',
                                            function($scope, $location, $routeParams, questionsService) {

        this.filterType = $routeParams.type ? $routeParams.type.toLowerCase() : null;
        this.questions = [];
        
        questionsService.getAllQuestions(this.filterType)
            .then(questions => {
                this.questions = questions;
            })
            .catch(data => {
                console.log('Error: ' + data);
            });

        this.addQuestion = function() {
            let question = { title: $scope.title, body: $scope.body, author: $scope.author };
            questionsService.addQuestion(question)
                .then(question => {
                    this.questions.push(question);
                })
                .catch(data => {
                    console.log('Error: ' + data);
                });
            $location.path('/questions');
        }
    }]);

    angular.module('gardenizeApp').controller('QuestionDetailsController', [
                                            '$scope',
                                            '$location',
                                            '$routeParams',
                                            'questionsService',
                                            function($scope, $location, $routeParams, questionsService) {
        
        let questionId = $routeParams.id;
        
        questionsService.getQuestion(questionId)
            .then(question => {
                this.question = question;
            })
            .catch(data => {
                console.log('Error: ' + data);
            });

        this.addComment = function() {
            let comment = { author: $scope.author, body: $scope.body };
            
            // clean form
            $scope.author = '';
            $scope.body = '';
            $scope.addComment.$setPristine();
            
            questionsService.addComment(questionId, comment)
                .then(question => {
                    this.question = question;
                })
                .catch(data => {
                    console.log('Error: ' + data);
                });
            $location.path('/questions/' + questionId);
        }
    }]);

}());