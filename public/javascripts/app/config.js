(function() {

    'use strict'

    angular.module('gardenizeApp').config(['$routeProvider', function($routeProvider) {

        $routeProvider

        .when('/', {
            templateUrl: '/static/partials/home.html',
            controller: 'HomePageController',
            controllerAs: 'homePageCtrl'
        })

        .when('/questions', {
            templateUrl: '/static/partials/questions/list.html',
            controller: 'QuestionsController',
            controllerAs: 'questionsCtrl'
        })

        .when('/questions/filter/:type', {
            templateUrl: '/static/partials/questions/list.html',
            controller: 'QuestionsController',
            controllerAs: 'questionsCtrl'
        })

        .when('/questions/new', {
            templateUrl: '/static/partials/questions/new.html',
            controller: 'QuestionsController',
            controllerAs: 'questionsCtrl'
        })

        .when('/questions/:id', {
            templateUrl: '/static/partials/questions/show.html',
            controller: 'QuestionDetailsController',
            controllerAs: 'questionDetailsCtrl'
        })

        .otherwise({ redirectTo: '/' });

    }]);

}());