'use strict'

let express = require('express');
let router = express.Router();

let controllers = require('../controllers/questions');


router.get('/', controllers.get_all_questions);
router.get('/filter/:type', controllers.get_questions_by_filter);
router.post('/', controllers.create_new_question);
router.get('/:id', controllers.get_question_by_id);
router.post('/:id/comments', controllers.create_new_comment);

// just for me to test via POSTMAN
router.delete('/', controllers.remove_all_questions);

module.exports = router;