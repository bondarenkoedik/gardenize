'use strict'

let sanitizeHtml = require('sanitize-html');

let Question = require('../models/Question');

let get_all_questions = function (req, res, next) {
    
    Question.find({})
        .then(questions => {
            return res.json(questions);
        })
        .catch(err => {
            return next(err);
        });

};

let get_questions_by_filter = function(req, res, next) {

    const filterType = req.params.type.toLowerCase();
    let searchFilter = {};

    if (filterType === 'answered') {
        searchFilter = { $where: 'this.comments.length > 0' };
    } else if (filterType === 'unanswered') {
        searchFilter = { $where: 'this.comments.length == 0' };
    }

    Question.find(searchFilter)
        .then(questions => {
            return res.json(questions);
        })
        .catch(err => {
            return next(err);
        });
};

let create_new_question = function (req, res, next) {

    // these three lines mutate req.body.title, req.body.body and req.body.author elements
    req.sanitize('title').trim();
    req.sanitize('body').trim();
    req.sanitize('author').trim();

    // so I decided to mutate these objects here too
    req.body.title = sanitizeHtml(req.body.title || '');
    req.body.body = sanitizeHtml(req.body.body || '');
    req.body.author = sanitizeHtml(req.body.author || '');

    req.assert('title', 'Enter a valid title for questions').notEmpty();
    req.assert('body', 'Enter a valid body for questions').notEmpty();
    req.assert('author', 'Enter a valid author for questions').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(400).json(errors);
    }

    Question.create(req.body)
        .then(question => {
            return res.json(question);
        })
        .catch(err => {
            return next(err);
        });

};

let get_question_by_id = function(req, res, next) {

    Question.findById(req.params.id)
        .then(question => {
            return res.json(question);
        })
        .catch(err => {
            // if id is not converted to Mongo::ObjectId show just empty result
            if (err.name === 'CastError') {
                return res.json();
            } else {
                return next(err);
            }
        });

};

let create_new_comment = function(req, res, next) {

    // these two lines mutate req.body.body and req.body.author elements
    req.sanitize('body').trim();
    req.sanitize('author').trim();

    // so I decided to mutate these objects here too
    req.body.body = sanitizeHtml(req.body.body || '');
    req.body.author = sanitizeHtml(req.body.author || '');

    req.assert('body', 'Enter a valid body for questions').notEmpty();
    req.assert('author', 'Enter a valid author for questions').notEmpty();

    let errors = req.validationErrors();
    if (errors) {
        return res.status(400).json(errors);
    }

    Question.findById(req.params.id)
        .then(question => {
            if (!question) {
                return res.status(400).send('Undefined question');
            } else {
                question.comments.push(req.body);
                return question.save();
            }
        })
        .then(question => {
            return res.json(question);
        })
        .catch(err => {
            return next(err);
        });

};

let remove_all_questions = function (req, res, next) {

    Question.remove()
        .then(() => { return res.json(); })
        .catch(err => { return res.json(err); });
};

module.exports = {
    get_all_questions,
    get_questions_by_filter,
    create_new_question,
    get_question_by_id,
    create_new_comment,
    remove_all_questions
};