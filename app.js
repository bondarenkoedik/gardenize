'use strict';

let express = require('express');
let path = require('path');
let favicon = require('serve-favicon');
let logger = require('morgan');
let cookieParser = require('cookie-parser');
let bodyParser = require('body-parser');
let mongoose = require('mongoose');
let validator = require('express-validator');

let routes = require('./routes/index');
let questions = require('./routes/questions');

let app = express();

// config

// development only
if ('development' === app.get('env')) {
  app.set('mongodb_uri', 'mongodb://localhost/gardenize_dev');
}

// production only
if ('production' === app.get('env')) {
  app.set('mongodb_uri', 'mongodb://localhost/gardenize_prod');
}

// database
mongoose.connect(app.get('mongodb_uri'));

mongoose.connection.on('connected', function () {
  console.log('Mongoose default connection open to ' + app.get('mongodb_uri'));
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
  console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
  console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close()
        .then(() => {
            console.log('Mongoose default connection disconnected through app termination');
            process.exit(0);
        })
        .catch((err) => {
            console.log(err);
            process.exit(1);
        });
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use('/static', express.static(path.join(__dirname, 'public')));
app.use(validator());

app.use('/', routes);
app.use('/questions', questions);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


module.exports = app;
