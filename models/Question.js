"use strict"

var mongoose = require("mongoose");

var QuestionSchema = new mongoose.Schema({
	title: String,
	body: String,
	author: String,
	date: { type : Date, default: Date.now },
	comments: [{
		author: String,
		body: String,
		date: { type : Date, default: Date.now }
	}]
});

var Question = mongoose.model("Question", QuestionSchema);

module.exports = Question;